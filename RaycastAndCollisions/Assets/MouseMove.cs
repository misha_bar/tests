﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour {

	// float speed = 10.0f;

	void Update () {
		// if (Input.GetAxis("Mouse X") > 0)
		// {
		//     transform.position += new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
		//                                0.0f, Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed);
		// }

		// else if (Input.GetAxis("Mouse X") < 0)
		// {
		//     transform.position += new Vector3(Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
		//                                0.0f, Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed);
		// }

		float mouseX = (Input.mousePosition.x / Screen.width) - 0.5f;
		float mouseY = (Input.mousePosition.y / Screen.height) - 0.5f;
		transform.localRotation = Quaternion.Euler (new Vector4 (-1f * (mouseY * 180f), mouseX * 360f, transform.localRotation.z));

	}
}