﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button3D : MonoBehaviour {


	public string text;
	public delegate void onClick();

	public delegate void ClickAction();
    public event ClickAction OnClicked;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Find("Text").GetComponent<TextMesh>().text = text;
	}

	void OnMouseEnter(){
		// print("OnMouseEnter");
		this.transform.Find("Press").GetComponent<MeshRenderer>().material.color=Color.gray;
	}

	void OnMouseExit(){
		// print("OnMouseExit");
		this.transform.Find("Press").GetComponent<MeshRenderer>().material.color=Color.white;
	}

	void OnMouseDown(){
		this.transform.Find("Press").GetComponent<MeshRenderer>().material.color=Color.red;
		this.transform.Find("Press").transform.Translate(new Vector3(0,-0.1f,0));
		// print("OnMouseDown");
		if(this.OnClicked!=null){
			this.OnClicked();
		}
	}

	void OnMouseUp(){
		this.transform.Find("Press").transform.Translate(new Vector3(0,0.1f,0));
		this.transform.Find("Press").GetComponent<MeshRenderer>().material.color=Color.gray;
	}

}
