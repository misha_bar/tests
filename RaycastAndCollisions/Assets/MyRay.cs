﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class MyRay : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (SteamVR_Input._default.inActions.Squeeze.GetAxis (SteamVR_Input_Sources.Any) > 0) {
			// print ("Squeeze");
			RaycastHit hit;
			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), out hit)) {
				Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * hit.distance, Color.yellow);
				//Debug.Log ("Hit" + hit.collider.gameObject.name);
				print (hit.point);
				GameObject.Find("PointerCube").transform.localPosition=hit.point;
			} else {
				Debug.DrawRay (transform.position, transform.TransformDirection (Vector3.forward) * 1000, Color.white);
				// Debug.Log ("Did not Hit");
			}
		}

		// for (int actionIndex = 0; actionIndex < SteamVR_Input.actionsIn.Length; actionIndex++) {
		// 	SteamVR_Action_In action = (SteamVR_Action_In) SteamVR_Input.actionsIn[actionIndex];
		// 	print ("action=" + action);
		// 	// if (action.GetActive (hand.handType)) {
		// 	// }
		// }
	}
}