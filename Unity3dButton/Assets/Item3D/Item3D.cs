﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item3D : MonoBehaviour {

	public string frontImage="http://mishabar.com:4007/munchkin/door_3.jpg";
	public string backImage="http://mishabar.com:4007/munchkin/door_back.jpg";
	// public float w2hRatio=1; // 399/620

	public string[] allowDropZones;

	// Use this for initialization
	void Start () {
		StartCoroutine(loadImageFront(this.frontImage));
		StartCoroutine(loadImageBack(this.backImage));

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	IEnumerator loadImageFront(string url){
		WWW www = new WWW(url);
		yield return www;

		// www.LoadImageIntoTexture(this.transform.Find("FrontImage").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
		this.transform.Find("FrontImage").GetComponent<MeshRenderer>().material.mainTexture = www.texture;
		this.transform.Find("FrontImage").GetComponent<MeshRenderer>().material.color = Color.white;

		print("w="+www.texture.width+" h="+www.texture.height);
		float r =  ((float) www.texture.height) / ((float) www.texture.width);
		print("r="+ r);
		this.transform.localScale = new Vector3(this.transform.localScale.x,this.transform.localScale.y,this.transform.localScale.z*r);
		
		yield return www;

	}

	IEnumerator loadImageBack(string url){
		WWW www = new WWW(url);
		yield return www;

		// www.LoadImageIntoTexture(this.transform.Find("FrontImage").GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
		this.transform.Find("BackImage").GetComponent<MeshRenderer>().material.mainTexture = www.texture;
		this.transform.Find("BackImage").GetComponent<MeshRenderer>().material.color = Color.white;
		yield return www;

	}
}
